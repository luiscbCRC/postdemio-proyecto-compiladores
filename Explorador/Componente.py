# En este archivo se encuentra toda la información relevante para trabajar con los componentes

# Se importa la biblioteca enum para poder generar los tipos y asignarles el valor según el nombre y no según un número

import enum
from re import match

class Componente(enum.Enum):
  """
  Clase que funciona como un enumerador para manejar los tipos de los componentes.

  ...

  Atributos
  ----------
  Al ser una clase de tipo Enumerador, solo cuenta con los atributos que se asignen para cada caso según \
  el tipo de componente.

  Métodos
  -------
  _generate_next_value_
    Esta funcion es un overwrite del método auto de la clase enum para darle un valor más significativo \
    a cada tipo de componente. Se le brinda el valor del mismo nombre pero en minúscula.
  """

  # Overwrite del método auto para hacer uso del nombre del enum como identificador
  def _generate_next_value_(name, start, count, last_values):
    return name.lower()
        
  COMENTARIO = enum.auto()
  ESPACIO = enum.auto()
  FUNCIONES_ESTANDAR = enum.auto()
  SEPARADORES = enum.auto()
  PUNTUACION = enum.auto()
  PALABRA_CLAVE = enum.auto()
  ITERACION = enum.auto()
  CONDICIONAL = enum.auto()
  COMPARADOR = enum.auto()
  ENTERO = enum.auto()
  FLOTANTE = enum.auto()
  CADENA_CARACTERES = enum.auto()
  IDENTIFICADOR = enum.auto()
  ASIGNACION = enum.auto()
  OPERADOR = enum.auto()
  ERROR_FLOTANTE = enum.auto()
  ERROR_IDENTIFICADOR = enum.auto()
  ERROR_CARACTER = enum.auto()
  ERROR_SINTAXIS = enum.auto()

class Regex:
  """
  La clase Regex maneja todas las funcionalidades de expresiones regulares que se van a ocupar \
  en el proyecto.

  ...

  Atributos
  ----------
  expresiones_regulares : lista de tuplas
    Almacena los tipos de componentes junto con su respectiva expresion regular.

  Métodos
  -------
  encontrar_incidencia(expresion:string, subseccion:string):
    Encuentra incidencias de expresiones regulares de un string.
  """

  def __init__(self):
    """
    Constructor para inicializar las expresiones regulares en conjunto con su tipo de componente

    Parámetros
    ----------
    Ninguno
    """
    self.expresiones_regulares = [
      # General
      (Componente.COMENTARIO, r'^antivacuna.*'),
      (Componente.ESPACIO, r'^(\s)+'),
      (Componente.FUNCIONES_ESTANDAR, r'^(elMinistroSalasDijo|vacuna|cantidadDeCasos|obtenerCaso|casosDeHoy)'),
      (Componente.SEPARADORES, r'^[(){}[\]]'),
      (Componente.PUNTUACION, r'^[,]'),  
      (Componente.PALABRA_CLAVE, r'^(covid|pandemia|delta|omicron|ejecutar|quitarMascarilla|toser|dadoDeAlta)'),
      # Estructuras de Control
      (Componente.ITERACION, r'^(cuarentena)'),
      (Componente.CONDICIONAL, r'^(contagio|curado)'),
      (Componente.COMPARADOR, r'^(masCasos|menosCasos|igualOMenosCasos|igualOMasCasos|mismosCasos|diferentesCasos)'),
      # Datos
      (Componente.FLOTANTE, r'^(-?([0-9]+[.])[0-9]+)'),
      (Componente.ERROR_FLOTANTE, r'^((-?[0-9]+\.(\S)*)|(-?\D*\.[0-9]+))'), #Error flotante
      (Componente.ENTERO, r'^(-?[0-9]+)'),
      (Componente.CADENA_CARACTERES, r'^(\".?[^$]*)\"'),
      # Manejo de Datos
      (Componente.ASIGNACION, r'^virus'),
      (Componente.OPERADOR, r'^(\+|-|\*|/|\*\*|%)'),
      (Componente.IDENTIFICADOR, r'^([a-z]([a-zA-Z0-9\_])*)'),
      (Componente.ERROR_IDENTIFICADOR, r'^([^A-Z0-9\s\.]+[a-zA-Z0-9\_]+)'), #Error identificador
      # Otros Errores
      (Componente.ERROR_CARACTER, r'^([^A-Za-z\s:.]+)') #Error de Caracter
      ]

  def encontrar_incidencia(self, expresion, subseccion):
    """
    Busca a partir de una expresion regular y un string, la incidencia que se especifica en la expresión.

    Parámetros
    ----------
    expresion : string, requerido
      Expresion regular para buscar en una subseccion especifica.

    subseccion : string, requerido
      String en el que se desea buscar alguna incidencia según la expresión regular escogida.

    Retorno
    -------
    Objeto de la clase re o None si no se encuentra ninguna incidencia.
    """

    return match(expresion, subseccion)


class Componente_objeto:
  """
    Clase que permite crear un objeto componente.

    Atributos
    tipo: el tipo de componente.
    string: el string asociado al componente.
    ----------

    Métodos
    -------

    """
  def __init__ (self, tipo, string, columna, fila):
      self.tipo = tipo
      self.string = string
      self.columna = columna
      self.fila = fila

class Error(Componente_objeto):
  """
    Clase que contiene un diccionario como atributo, que asocia los tipos de error de cada componente
    con su respectivo mensaje.

    Atributos
    ErrorMSG: diccionario que asocia los tipos de error de cada componente
    con su respectivo mensaje.
    ----------

    Métodos
    -------
    
    """
  ErrorMSG = {
    Componente.ERROR_IDENTIFICADOR: "No se puede utilizar este tipo de caracteres al inicio de la definicion de una variable.",
    Componente.ERROR_FLOTANTE: "Hay una mala definicion del flotante.",
    Componente.ERROR_CARACTER: "Revise bien este caracter :(\n\tSugerencia: Elimine el caracter.",
    Componente.ERROR_SINTAXIS: "Explotó, solo corra."
  }

  def __init__(self, tipo, fila, columna, string):
    self.tipo = tipo
    self.fila = fila
    self.columna = columna
    self.string = string