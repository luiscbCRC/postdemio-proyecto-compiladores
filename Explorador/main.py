# Archivo que funciona de punto de inicio para el explorador

# Se importan bibliotecas importantes para trabajar con expresiones regulares y la información de los componentes

from re import match
from Explorador.Componente import Componente, Regex, Componente_objeto, Error

class Explorador:
  """
  La clase explorador maneja toda la funcionalidad del explorador del programa.

  ...

  Atributos
  ----------
  regex : Regex
    Clase regex para utilizar funcionalidades de expresiones regulares.
  codigo : lista
    Todo el codigo del archivo pdemio almacenado en una lista.
  numero_linea : int
    Numero de linea para efectos informativos y despliegue de errores.
  numero_columna : int
    Numero de columna para efectos informativos y despliegue de errores.
  componentes_léxicos : lista
    Todos los componentes léxicos que fueron encontrados en el programa. Cada item es un objeto de tipo re.

  Métodos
  -------
  inicio():
    Punto de entrada del explorador.
  buscar_componente(linea:string):
    Loop que busca componentes a lo largo de una linea.
  """


  def __init__(self, codigo):
    """
    Constructor necesario para inicializar variables esenciales del explorador.

    Parámetros
    ----------
    codigo : lista, requerido
      Una lista con con el código que se desea analizar. Cada item es una linea de código.
    """
    self.regex = Regex()
    self.codigo = codigo
    self.numero_linea = 0
    self.numero_columna = 0
    self.componentes_lexicos = []
    self.errores = []
    self.explorar()

  def inicio(self):
    self.imprimir_componentes()
    self.imprimir_errores()

  def explorar(self):
    """
    Punto de entrada para ejecutar todas las funcionalidades y dependencias del explorador.

    Parámetros
    ----------
    Ninguno

    Retorno
    -------
    Una lista con los componentes léxicos.
    """
    for linea in self.codigo:
      self.numero_linea += 1
      if linea != "\n" and len(linea) != 1:
        self.buscar_componentes(linea)
      self.numero_columna = 0

    return self.componentes_lexicos


  def imprimir_componentes(self):
    """
    Imprime la lista de componentes en terminal

    Parámetros
    ----------
    Ninguno

    Retorno
    -------
    Ninguno
    """
    if len(self.componentes_lexicos) == 0:
      print("No hay componentes")
    else:
      for componente in self.componentes_lexicos:
        print(componente.tipo.name, end=" : ")
        print(componente.string)

  def imprimir_errores(self):
    """
    Imprime la lista de componentes en terminal

    Parámetros
    ----------
    Ninguno

    Retorno
    -------
    Ninguno
    """
    if len(self.errores) == 0:
      print("No hay errores")
    else:
      for error in self.errores:
        if error.tipo is Componente.ERROR_SINTAXIS:
          print(error.tipo.name + " : " + Error.ErrorMSG.get(error.tipo) + "\n\tFila #" + str(error.fila)
                + ", Columna #" + str(error.columna))
        else:
          print(error.tipo.name + " : " + Error.ErrorMSG.get(error.tipo), end="\n\t")
          print("Expresion: \"" + error.string + "\", Fila #" + str(error.fila)
                + ", Columna #" + str(error.columna))

  def obtener_componentes(self):
    return self.componentes_lexicos
  
  def buscar_componentes(self, linea):
    """
    Este es un loop que se encarga de a partir de todos los tipos de componentes, analizar una linea y \
    encontrar incidencias de esos componentes a lo largo de una única línea

    Parámetros
    ----------
    linea : string, requerido
      Linea de código en string.

    Retorno
    -------
    Ninguno
    """
    tmp_list = []

    while(self.numero_columna < len(linea)):
      subseccion = linea[self.numero_columna:]
      tmp = self.numero_columna
      for componente, expresion_regular in self.regex.expresiones_regulares:
        componente_encontrado = self.regex.encontrar_incidencia(expresion_regular, subseccion)
        if componente_encontrado is not None:
          self.numero_columna += componente_encontrado.end()
          if componente is Componente.COMENTARIO or componente is Componente.ESPACIO:
            break
          elif componente is Componente.ERROR_FLOTANTE or \
                  componente is Componente.ERROR_IDENTIFICADOR or \
                  componente is Componente.ERROR_CARACTER:
            error = Error(componente, self.numero_linea, self.numero_columna, componente_encontrado.group())
            self.errores.append(error)
            break
          else:
            objeto = Componente_objeto(componente, componente_encontrado.group(), self.numero_columna, self.numero_linea)
            tmp_list.append(objeto)
            break

      if(tmp == self.numero_columna):
        self.errores.append(Error(Componente.ERROR_SINTAXIS, self.numero_linea, self.numero_columna, componente_encontrado))
        break

    self.componentes_lexicos.extend(tmp_list[::-1])