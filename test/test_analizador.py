import unittest

import Archivo as archivo
import Explorador.main as exp
import Analizador.main as analizador
from Analizador.Arbol.arbol import ArbolSintaxisAbstracta

class TestAnalizador(unittest.TestCase):
    """
      Clase que permite realizar las pruebas necesarias para el funcionamiento correcto del analizador.

      ...

      Atributos
      ----------

      Métodos
      -------
      testFileWithNoError (1,2,3,4,5):
        Pruebas que analizan archivos sin ningún tipo de error a nivel de analizador.
      testFileWithError (1,2,3,4,5,6):
        Pruebas que analizan un archivo con algún tipo de error a nivel de analizador.
    """


    cargarArchivo = archivo.CargarArchivo()

    def testFileWithNoError1(self):
        """
        A partir de un archivo previamente definido sin errores
        se encarga de analizar que el analizador los maneja correctamente y no crea ningún error.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo sin Errores 1 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/NoError/fibonacci.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)
        arbol = analizar.inicio()
        print(type(arbol))
        self.assertEqual(type(arbol), ArbolSintaxisAbstracta)

    def testFileWithNoError2(self):
        """
        A partir de un archivo previamente definido sin errores
        se encarga de analizar que el analizador los maneja correctamente y no crea ningún error.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo sin Errores 2 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/NoError/factorial.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)
        arbol = analizar.inicio()
        self.assertEqual(type(arbol), ArbolSintaxisAbstracta)

    def testFileWithNoError3(self):
        """
        A partir de un archivo previamente definido sin errores
        se encarga de analizar que el analizador los maneja correctamente y no crea ningún error.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo sin Errores 3 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/NoError/invertir.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)
        arbol = analizar.inicio()
        self.assertEqual(type(arbol), ArbolSintaxisAbstracta)

    def testFileWithNoError4(self):
        """
        A partir de un archivo previamente definido sin errores
        se encarga de analizar que el analizador los maneja correctamente y no crea ningún error.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo sin Errores 4 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/NoError/substring.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)
        arbol = analizar.inicio()
        self.assertEqual(type(arbol), ArbolSintaxisAbstracta)

    def testFileWithNoError5(self):
        """
        A partir de un archivo previamente definido sin errores
        se encarga de analizar que el analizador los maneja correctamente y no crea ningún error.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo sin Errores 4 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/NoError/quitar_pares.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)
        arbol = analizar.inicio()
        self.assertEqual(type(arbol), ArbolSintaxisAbstracta)


    def testFileWithError1(self):
        """
        A partir de un archivo previamente definido con errores
        se encarga de verificar que el analizador los maneja correctamente
        y reporta los errores respectivos.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo con Errores 1 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/TestAnalizador/fibonacciError.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)

        with self.assertRaises(SystemExit) as error:
            analizar.inicio()

        self.assertEqual(error.exception.code, 1)

    def testFileWithError2(self):
        """
        A partir de un archivo previamente definido con errores
        se encarga de verificar que el analizador los maneja correctamente
        y reporta los errores respectivos.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo con Errores 2 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/TestAnalizador/factorialError.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)

        with self.assertRaises(SystemExit) as error:
            analizar.inicio()

        self.assertEqual(error.exception.code, 1)

    def testFileWithError3(self):
        """
        A partir de un archivo previamente definido con errores
        se encarga de verificar que el analizador los maneja correctamente
        y reporta los errores respectivos.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo con Errores 3 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/TestAnalizador/invertirError.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)

        with self.assertRaises(SystemExit) as error:
            analizar.inicio()

        self.assertEqual(error.exception.code, 1)

    def testFileWithError4(self):
        """
        A partir de un archivo previamente definido con errores
        se encarga de verificar que el analizador los maneja correctamente
        y reporta los errores respectivos.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo con Errores 4 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/TestAnalizador/quitarParesError.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)

        with self.assertRaises(SystemExit) as error:
            analizar.inicio()

        self.assertEqual(error.exception.code, 1)

    def testFileWithError5(self):
        """
        A partir de un archivo previamente definido con errores
        se encarga de verificar que el analizador los maneja correctamente
        y reporta los errores respectivos.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo con Errores 5 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/TestAnalizador/substringError.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)

        with self.assertRaises(SystemExit) as error:
            analizar.inicio()

        self.assertEqual(error.exception.code, 1)

    def testFileWithError6(self):
        """
        A partir de un archivo previamente definido con errores
        se encarga de verificar que el analizador los maneja correctamente
        y reporta los errores respectivos.

        Parámetros
        ----------

        Retorno
        -------
        """
        print("--------------------------------------------------------------")
        print("\nTest Analizador: Archivo con Errores 6 - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/TestAnalizador/fibonacciError2.pdemio"
        codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        analizar = analizador.Analizador(explorador.componentes_lexicos)

        with self.assertRaises(SystemExit) as error:
            analizar.inicio()

        self.assertEqual(error.exception.code, 1)


if __name__ == '__main__':
    unittest.main()
