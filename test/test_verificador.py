import unittest

from Analizador.Arbol.arbol import ArbolSintaxisAbstracta, TipoNodo, NodoArbol
from Verificador.TablaSimbolos import TablaSimbolos
from Verificador.TipoDatos.tiposDatos import TipoDatos
from Verificador.Visitante import Visitante
import Archivo as archivo
import Explorador.main as exp
import Analizador.main as analizador
from Analizador.Arbol.arbol import ArbolSintaxisAbstracta
from Generador.main import Generador

from Explorador.main import Explorador
from Analizador.main import Analizador
from Verificador.main import Verificador
from Generador.main import Generador
from Archivo import CargarArchivo

class TestExplorador(unittest.TestCase):
  """
    Clase que permite realizar las pruebas necesarias para el funcionamiento correcto del analizador.

    ...

    Atributos
    ----------

    Métodos
    -------
    testFileWithNoError (1,2,3,4,5):
      Pruebas que analizan archivos sin ningún tipo de error a nivel de analizador.
    testFileWithError (1,2,3,4,5,6):
      Pruebas que analizan un archivo con algún tipo de error a nivel de analizador.
  """

  self.cargarArchivo = CargarArchivo()

  def testFibonacciVariable(self):
  """
    A partir de un archivo previamente definido sin errores
    se encarga de verificar que el explorador los maneja correctamente y no crea ningún error.

    Parámetros
    ----------

    Retorno
    -------
  """
    print("--------------------------------------------------------------")
    print("\nTest Analizador: Archivo con Errores 5 - Lenguaje Postdemio")
    rutaArchivo = "../Postdemio/TestVerificador/fibonacciVariable.pdemio"
    codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
    explorador = Explorador(codigo)
    explorador.inicio()
    analizar = Analizador(explorador.componentes_lexicos)
    analizador.inicio()
    verificador = Verificador(self.analizador.obtener_arbol())
    
    with self.assertRaises(SystemExit) as error:
      verificador.inicio()

    self.assertEqual(error.exception.code, 1)

  def testFibonacciTipo(self):
  """
    A partir de un archivo previamente definido sin errores
    se encarga de verificar que el explorador los maneja correctamente y no crea ningún error.

    Parámetros
    ----------

    Retorno
    -------
  """
    print("--------------------------------------------------------------")
    print("\nTest Analizador: Archivo con Errores 5 - Lenguaje Postdemio")
    rutaArchivo = "../Postdemio/TestVerificador/fibonacciTipo.pdemio"
    codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
    explorador = Explorador(codigo)
    explorador.inicio()
    analizar = Analizador(explorador.componentes_lexicos)
    self.analizador.inicio()
    self.verificador = Verificador(self.analizador.obtener_arbol())
    with self.assertRaises(SystemExit) as error:
      verificador.inicio()

    self.assertEqual(error.exception.code, 1)


  def testFactorialVariable(self):
  """
    A partir de un archivo previamente definido sin errores
    se encarga de verificar que el explorador los maneja correctamente y no crea ningún error.

    Parámetros
    ----------

    Retorno
    -------
  """
    print("--------------------------------------------------------------")
    print("\nTest Analizador: Archivo con Errores 5 - Lenguaje Postdemio")
    rutaArchivo = "../Postdemio/TestVerificador/factorialVariable.pdemio"
    codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
    explorador = Explorador(codigo)
    explorador.inicio()
    analizar = Analizador(explorador.componentes_lexicos)
    self.analizador.inicio()
    self.verificador = Verificador(self.analizador.obtener_arbol())
    with self.assertRaises(SystemExit) as error:
      verificador.inicio()

    self.assertEqual(error.exception.code, 1)


  def testFactorialTipo(self):
  """
    A partir de un archivo previamente definido sin errores
    se encarga de verificar que el explorador los maneja correctamente y no crea ningún error.

    Parámetros
    ----------

    Retorno
    -------
  """
    print("--------------------------------------------------------------")
    print("\nTest Analizador: Archivo con Errores 5 - Lenguaje Postdemio")
    rutaArchivo = "../Postdemio/TestVerificador/factorialTipo.pdemio"
    codigo = self.cargarArchivo.archivo_a_lista(rutaArchivo)
    explorador = Explorador(codigo)
    explorador.inicio()
    analizar = Analizador(explorador.componentes_lexicos)
    self.analizador.inicio()
    self.verificador = Verificador(self.analizador.obtener_arbol())
    with self.assertRaises(SystemExit) as error:
      verificador.inicio()

    self.assertEqual(error.exception.code, 1)

if __name__ == '__main__':
    unittest.main()
