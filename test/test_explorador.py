# En este archivo se realizan las pruebas unitarias para el explorador.

# Se importa la biblioteca unittest para una mayor presición en las pruebas

import unittest
import Archivo as archivo
import Explorador.main as exp


class TestExplorador(unittest.TestCase):
    """
      Clase que permite realizar las pruebas necesarias para el funcionamiento correcto del explorador.

      ...

      Atributos
      ----------

      Métodos
      -------
      testFileNoErrors:
        Prueba que se explore un archivo sin ningún tipo de error que maneja el explorador.
      testFileWithErrors:
        Prueba que se explore un archivo con algún tipo de error que maneja el explorador.
      testFileToList:
        Prueba que la carga del contenido del archivo a una lista sea correcta.
      testEmptyFile:
        Prueba que se explore un archivo con ningún contenido dentro.
    """
    def testFileNoErrors(self):
        """
        A partir de un archivo previamente definido sin errores
        se encarga de verificar que el explorador los maneja correctamente y no crea ningún error.

        Parámetros
        ----------

        Retorno
        -------
        """
        rutaArchivo = "../Postdemio/TestExplorador/substring.pdemio"
        cargarArchivo = archivo.CargarArchivo()
        codigo = cargarArchivo.archivo_a_lista(rutaArchivo)
        print("\nTest: Archivo sin Errores - Lenguaje Postdemio")
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        explorador.imprimir_errores()
        self.assertEqual(len(explorador.errores), 0)

    def testFileWithErrors(self):      
        """
        A partir de un archivo previamente definido con errores
        se encarga de verificar que el explorador los crea e imprime correctamente.

        Parámetros
        ----------

        Retorno
        -------

        """
        
        rutaArchivo = "../Postdemio/TestExplorador/factorialError.pdemio"
        cargarArchivo = archivo.CargarArchivo()
        codigo = cargarArchivo.archivo_a_lista(rutaArchivo)
        print("\nTest: Archivo con Errores - Lenguaje Postdemio")
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        explorador.imprimir_errores()
        self.assertNotEqual(len(explorador.errores), 0)

    def testFileToList(self):
        """
        Verifica que el contenido de un archivo se cargue correctamente en la lista para ser procesado.

        Parámetros
        ----------

        Retorno
        -------

        """
        print("\nTest: Archivo a Lista - Lenguaje Postdemio")
        rutaArchivo = "../Postdemio/TestExplorador/factorialError.pdemio"
        cargarArchivo = archivo.CargarArchivo()
        codigo = cargarArchivo.archivo_a_lista(rutaArchivo)
        print(codigo)
        self.assertTrue(len(codigo) > 0)

    def testEmptyFile(self):
        """
        A partir de un archivo previamente definido sin ningún contenido
        se encarga de verificar que el explorador maneja correctamente el mismo.

        Parámetros
        ----------

        Retorno
        -------

        """
        rutaArchivo = "../Postdemio/TestExplorador/archivoVacio.pdemio"
        cargarArchivo = archivo.CargarArchivo()
        codigo = cargarArchivo.archivo_a_lista(rutaArchivo)
        print("\nTest: Archivo Vacio - Lenguaje Postdemio")
        explorador = exp.Explorador(codigo)
        explorador.inicio()
        explorador.imprimir_componentes()
        explorador.imprimir_errores()
        self.assertEqual(len(explorador.componentes_lexicos), 0)
        self.assertEqual(len(explorador.errores), 0)

if __name__ == '__main__':
    unittest.main()
