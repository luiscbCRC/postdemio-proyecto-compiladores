import sys
import random as rand
def elMinistroSalasDijo(texto):
	print(texto)
def casosDeHoy(min, max):
	return rand.randint(min, max)
def vacuna(string):
	return input(string)
def cantidadDeCasos(string):
	return len(string)
def obtenerCaso(string, entero):
	return string[entero]

def carreraCaracoles (meta):
	carreraFinalizada = 0
	caracol1 = 0
	caracol2 = 0
	movimientos1 = 0
	movimientos2 = 0
	while carreraFinalizada == 0:
		nuevosMovimientos = casosDeHoy(1, 3)
		if caracol1 < caracol2:
			movimientos1 = movimientos1 + nuevosMovimientos
			caracol1 = caracol1 + 1
		else:
			movimientos2 = movimientos2 + nuevosMovimientos
			caracol2 = caracol2 + 1 
		imprimirMovimientos(movimientos2, movimientos1)
		carreraFinalizada = verificarFinalizado(meta, movimientos1)
		if carreraFinalizada == 0:
			carreraFinalizada = verificarFinalizado(meta, movimientos2)
def imprimirMovimientos (movimientos2,movimientos1):
	resultado1 = "Caracol 1:"
	resultado2 = "Caracol 2:"
	while movimientos1 != 0:
		resultado1 = resultado1 + "-"
		movimientos1 = movimientos1 - 1
	while movimientos2 != 0:
		resultado2 = resultado2 + "-"
		movimientos2 = movimientos2 - 1
	resultado1 = resultado1 + "@"
	resultado2 = resultado2 + "@"
	elMinistroSalasDijo("")
	elMinistroSalasDijo(resultado1)
	elMinistroSalasDijo(resultado2)
def verificarFinalizado (meta,movimientos):
	if movimientos < meta:
		return 0
	return 1
if __name__ == '__main__':
	carreraCaracoles(20)
