
from tokenize import String


class TablaSimbolos:

    simbolos: list = []
    errores: list = []
    profundidad: int = 0

    def abrir_bloque(self):
        self.profundidad += 1

    def cerrar_bloque(self):
        aux = []
        for registro in self.simbolos:
            if registro['profundidad'] != self.profundidad:
                aux.append(registro)
    
        self.simbolos = aux
        self.profundidad -= 1

    def registrar_simbolo(self, nodo):        
            
        diccionario = {}
        if(type(nodo.contenido) is str):
            diccionario['nombre']     = nodo.contenido 
        if(type(nodo.contenido) is not str and nodo.contenido is not None):
            if(self.verificar_existencia(nodo.contenido.string) is not None):
                return None
            diccionario['nombre']     = nodo.contenido.string
        diccionario['tipo']      = nodo.tipo           #Revisar
        diccionario['profundidad'] = self.profundidad
        diccionario['referencia']  = nodo
        diccionario['atributos'] = nodo.atributos

        self.simbolos.append(diccionario)
        print(self.__str__())


    def verificar_existencia(self, nombre):

        for registro in self.simbolos:

            if registro['nombre'] == nombre and registro['profundidad'] <= self.profundidad:
                return registro

        self.errores.append('El siguiente identificador no està definido' + nombre)

    def verificar_referencia(self, nombre):

        for registro in self.simbolos:

            if registro['nombre'] == nombre and registro['profundidad'] <= self.profundidad:
                return registro['referencia']

    def verificar_atributo(self, nombre):

        for registro in self.simbolos:

            if registro['nombre'] == nombre and registro['profundidad'] <= self.profundidad:
                return registro['atributos']

    def imprimir(self):
        print(self.__str__())


    def __str__(self):
        resultado = 'TABLA DE SÍMBOLOS\n\n'
        resultado += 'Profundidad: ' + str(self.profundidad) +'\n\n'
        for registro in self.simbolos:
            resultado += str(registro) + '\n'
        
        return resultado