from Analizador.Arbol.arbol import TipoNodo
from Verificador import TablaSimbolos
from Verificador.TipoDatos.tiposDatos import TipoDatos
import sys

class Visitante:

  tablaSimbolos: TablaSimbolos

  def __init__(self, nuevo_tabla_simbolos):
    self.tablaSimbolos = nuevo_tabla_simbolos


  def visitar(self, nodo: TipoNodo): 
    if nodo.tipo is TipoNodo.PROGRAMA:      
      self.__visitar_programa(nodo)

    elif nodo.tipo is TipoNodo.ASIGNACION:
      self.__visitar_asignacion(nodo)

    elif nodo.tipo is TipoNodo.IDENTIFICADOR:
      self.__visitar_identificador(nodo)

    elif nodo.tipo is TipoNodo.ENTERO:
      self.__visitar_entero(nodo)

    elif nodo.tipo is TipoNodo.FLOTANTE:
      self.__visitar_flotante(nodo)

    elif nodo.tipo is TipoNodo.TEXTO:
      self.__visitar_texto(nodo)

    elif nodo.tipo is TipoNodo.INVOCACION:
      self.__visitar_invocacion(nodo)
    elif nodo.tipo is TipoNodo.PARAMETROS:
      self.__visitar_parametro(nodo)

    elif nodo.tipo is TipoNodo.REFERENCIAS:
      self.__visitar_referencias(nodo)

    elif nodo.tipo is TipoNodo.OPERACION:
      self.__visitar_operacion(nodo)

    elif nodo.tipo is TipoNodo.OPERADOR:
      self.__visitar_operador(nodo)

    elif nodo.tipo is TipoNodo.FUNCION:
      self.__visitar_funcion(nodo)


    elif nodo.tipo is TipoNodo.INSTRUCCION:
      self.__visitar_instruccion(nodo)

    elif nodo.tipo is TipoNodo.CONDICION:
      self.__visitar_condicion(nodo)

    elif nodo.tipo is TipoNodo.SI:
      self.__visitar_si(nodo)

    elif nodo.tipo is TipoNodo.SINO:
      self.__visitar_sino(nodo)

    elif nodo.tipo is TipoNodo.COMPARACION:
      self.__visitar_comparacion(nodo)

    elif nodo.tipo is TipoNodo.COMPARADOR:
      self.__visitar_comparador(nodo)

    elif nodo.tipo is TipoNodo.RETORNO:
      self.__visitar_retorno(nodo)

    elif nodo.tipo is TipoNodo.ITERACION:
      self.__visitar_iteracion(nodo)

    elif nodo.tipo is TipoNodo.PRINCIPAL:
      self.__visitar_principal(nodo)

    elif nodo.tipo is TipoNodo.OPERADOR_LOGICO:
      self.__visitar_operador_logico(nodo)

    elif nodo.tipo is TipoNodo.AMBIENTE_ESTANDAR:
      self.__visitar_ambiente_estandar(nodo)

    elif nodo.tipo is TipoNodo.OPERANDO:
      self.__visitar_operando(nodo)

    elif nodo.tipo is TipoNodo.VARIABLE:
      self.__visitar_variable(nodo)

    elif nodo.tipo is TipoNodo.CONTENIDO:
      self.__visitar_contenido(nodo)

    else:
      print('')

  def __visitar_parametrosRetorno(self,nodo):
    return None


  def __visitar_programa(self, actual):
    """
    Programa ::= (Asignación | Invocaciones | Comentario | Función)* covid Contenido
    """
    
    for nodo in actual.nodos:
      nodo.visitar(self)


  def __visitar_asignacion(self, actual):
    """
    Asignación ::= (Operación | Valor | Identificador | Invocaciones), Identificador virus
    """
    self.tablaSimbolos.registrar_simbolo(actual.nodos[0])

    for nodo in actual.nodos:
      if nodo.tipo == TipoNodo.IDENTIFICADOR:
        reg = self.tablaSimbolos.verificar_existencia(nodo.contenido.string)
        if(reg is None):
          self.manejoErrores(nodo, "Identificador no declarado")
      nodo.visitar(self)

    actual.atributos['tipo'] = actual.nodos[1].atributos['tipo']
    actual.nodos[0].atributos['tipo'] = actual.nodos[1].atributos['tipo']


  def __visitar_identificador(self, actual):
    """
    Identificador ::= [a-z] [a-z A-Z 0-9 _]*
    """
    reg = self.tablaSimbolos.verificar_existencia(actual.contenido.string)
    if reg is None:
      self.manejoErrores(actual, "Identificador no declarado")

    # Aqui se tiene que asignar el tipo
    reg = self.tablaSimbolos.verificar_referencia(actual.contenido.string)
    actual.atributos['tipo'] = reg.atributos.get('tipo')
    if  actual.atributos['tipo'] == None:
      actual.atributos['tipo'] = TipoDatos.CUALQUIERA


  def __visitar_entero(self, actual):
    """
    Entero ::= [-+]?[0-9]+
    """
    actual.atributos['tipo'] = TipoDatos.NÚMERO


  def __visitar_flotante(self, actual):
    """
    Flotante ::= [-+]?([0-9]+)?\.[0-9]+
    """
    actual.atributos['tipo'] = TipoDatos.NÚMERO


  def __visitar_texto(self, actual):
    """
    Texto ::= ([a-z] | [A-Z]) (texto)*
    """
    actual.atributos['tipo'] = TipoDatos.TEXTO


  def __visitar_invocacion(self, actual):
    """
    Invocaciones ::= (Parámetro?) Identificador ejecutar
    """

    for nodo in actual.nodos:    
      nodo.visitar(self)

    actual.atributos['tipo'] = actual.nodos[0].atributos['tipo']

  def __visitar_parametro(self, actual):
    """
    Parametro ::= Variable | Variable, Parámetro
    """    
    
    for nodo in actual.nodos:
      if nodo.nodos != []:
        if nodo.nodos[0].tipo == TipoNodo.IDENTIFICADOR:
          reg = self.tablaSimbolos.verificar_existencia(nodo.nodos[0].contenido.string)
          if(reg is None):
            self.manejoErrores(nodo.nodos[0], "Identificador no declarado")
      nodo.visitar(self)


  def __visitar_referencias(self, actual):
    """
    Referencia ::= Identificador | Identificador, Referencia
    """
    for nodo in actual.nodos:
      self.tablaSimbolos.registrar_simbolo(nodo)
      nodo.visitar(self)

    actual.atributos['tipo'] = TipoDatos.CUALQUIERA
    


  def __visitar_operacion(self, actual):
    """
    Operación ::= [Operando Operando Operador]
    """
    for nodo in actual.nodos:
      if nodo.nodos != []:
        if nodo.nodos[0].tipo == TipoNodo.IDENTIFICADOR:
          reg = self.tablaSimbolos.verificar_existencia(nodo.nodos[0].contenido.string)
          if(reg is None):
            self.manejoErrores(nodo.nodos[0], "Identificador no declarado")
      if nodo.tipo == TipoNodo.IDENTIFICADOR:
        reg = self.tablaSimbolos.verificar_existencia(nodo.contenido.string)
        if(reg is None):
          self.manejoErrores(nodo, "Identificador no declarado")
      nodo.visitar(self)

    if actual.nodos[0].atributos['tipo'] == TipoDatos.CUALQUIERA or actual.nodos[2].atributos['tipo'] == TipoDatos.CUALQUIERA:
      pass 
    elif actual.nodos[0].atributos['tipo'] != actual.nodos[2].atributos['tipo']:
      self.manejoErrores(actual.nodos[0].nodos[0], "Los tipos de la operación actual, no coincidennn")


    actual.atributos['tipo'] = TipoDatos.NÚMERO


  def __visitar_operador(self, actual):
    """
    Operador ::= + | - | * | / | % | ^
    """
    actual.atributos['tipo'] = TipoDatos.NÚMERO


  def __visitar_funcion(self, actual):    
    """
    Función :== (Referencia?) Identificador pandemia delta Instrucción* Retorno omicron
    """
    self.tablaSimbolos.registrar_simbolo(actual.nodos[0])  
    
    self.tablaSimbolos.abrir_bloque()

    for nodo in actual.nodos:      
      nodo.visitar(self)
    
    self.tablaSimbolos.cerrar_bloque()
    self.tablaSimbolos.imprimir()

    actual.atributos['tipo'] = actual.nodos[-1].atributos['tipo']


  def __visitar_instruccion(self, actual):
    """
    Instrucción ::= (Comentario | Asignación | Lógica | Invocaciones)
    """    
    for nodo in actual.nodos:
      nodo.visitar(self)
      actual.tipo = nodo.tipo
      actual.atributos['tipo'] = nodo.atributos['tipo']


  def __visitar_condicion(self, actual):
    """
    Condición ::= Si Sino?
    """
    for nodo in actual.nodos:
      nodo.visitar(self)

    actual.atributos['tipo'] = TipoDatos.CUALQUIERA 


  def __visitar_si(self, actual):
    """
    Si ::= Comparacion contagio Contenido
    """
    self.tablaSimbolos.abrir_bloque()

    for nodo in actual.nodos:
      nodo.visitar(self)

    self.tablaSimbolos.cerrar_bloque()
    self.tablaSimbolos.imprimir()

    actual.atributos['tipo'] = actual.nodos[1].atributos['tipo']


  def __visitar_sino(self, actual):
    """
    Sino ::= curado Contenido
    """
    self.tablaSimbolos.abrir_bloque()

    for nodo in actual.nodos:
      nodo.visitar(self)
    
    self.tablaSimbolos.cerrar_bloque()
    self.tablaSimbolos.imprimir()


  def __visitar_comparacion(self, actual):
    """
    Comparación ::= {Operando Operando Comparador}
    """

    # TODO: Esto es lo más pesado, hay que ver que Operando Operando sean del mismo tipo
    # comparacion[0] operando comparacion[1] operando

    for nodo in actual.nodos:
      if nodo.nodos != []:
        if nodo.nodos[0].tipo == TipoNodo.IDENTIFICADOR:
          reg = self.tablaSimbolos.verificar_existencia(nodo.nodos[0].contenido.string)
          if(reg is None):
            self.manejoErrores(nodo.nodos[0], "Identificador no declarado")
      if nodo.tipo == TipoNodo.IDENTIFICADOR:      
        reg = self.tablaSimbolos.verificar_existencia(nodo.contenido.string)
        if(reg is None):
          self.manejoErrores(nodo, "Identificador no declarado")
      nodo.visitar(self)
    if actual.nodos[0].atributos['tipo'] == TipoDatos.CUALQUIERA or actual.nodos[2].atributos['tipo'] == TipoDatos.CUALQUIERA:
      pass 
    elif actual.nodos[0].atributos['tipo'] != actual.nodos[2].atributos['tipo']:
      self.manejoErrores(actual.nodos[0].nodos[0], "Los tipos de la operación actual, no coincidenaa")

  def __visitar_comparador(self, actual):
    """
    Comparador ::= masCasos | menosCasos | igualOMenosCasos | igualOMasCasos | mismosCasos | diferentesCasos
    """
    actual.atributos['tipo'] = TipoDatos.CUALQUIERA


  def __visitar_retorno(self, actual):
    """
    Retorno ::= Variable toser
    """
      
    if actual.nodos == []:   
      # Si no retorna un valor no retorna un tipo específico 
      actual.atributos['tipo'] = TipoDatos.NINGUNO

    else:
      for nodo in actual.nodos:
        nodo.visitar(self)
        if nodo.tipo == TipoNodo.IDENTIFICADOR:
          registro = self.tablaSimbolos.verificar_existencia(nodo.contenido.string)
          if registro != None: 
            # le doy al sarpe el tipo de retorno del identificador encontrado
            actual.atributos['tipo'] = registro['referencia'].atributos['tipo']
          else:
            self.manejoErrores(nodo, "Identificador no declarado")

        else:
          # Verifico si es un Literal de que tipo es (TIPO)
          actual.atributos['tipo'] = nodo.atributos['tipo']
          


  def __visitar_iteracion(self, actual):
    """
    Iteracion ::= comparacion cuarentena Contenido
    """
    self.tablaSimbolos.abrir_bloque()

    for nodo in actual.nodos:
      nodo.visitar(self)

    self.tablaSimbolos.cerrar_bloque()
    self.tablaSimbolos.imprimir()

    actual.atributos['tipo'] = actual.nodos[1].atributos['tipo']


  def __visitar_principal(self, actual):
    """
    Funcion que visita la funcion principal
    """
    for nodo in actual.nodos:
      nodo.visitar(self)

    actual.atributos['tipo'] = actual.nodos[0].atributos['tipo']


  def __visitar_ambiente_estandar(self, actual):
    """
    Funcion que visita las funciones estandar
    """
    atributo = self.tablaSimbolos.verificar_atributo(actual.contenido.string)
    actual.atributos['tipo'] = atributo['tipo']


  def __visitar_operando(self, actual):
    """
    Operando ::= Identificador | Valor
    """
    for nodo in actual.nodos:
      nodo.visitar(self)

    actual.atributos['tipo'] = actual.nodos[0].atributos['tipo']


  def __visitar_variable(self, actual):
    """
    Variable ::= (Valor | Identificador)
    """
    if actual.nodos[0].tipo == TipoNodo.IDENTIFICADOR:
      registro = self.tablaSimbolos.verificar_existencia(actual.nodos[0].contenido.string)
      if registro is None:
        self.manejoErrores(actual.nodos[0], "Identificador no declarado")
      actual.atributos['tipo'] = TipoDatos.CUALQUIERA
    else:
      if actual.nodos[0].contenido.string.isnumeric():
        actual.atributos['tipo'] = TipoDatos.NÚMERO
      else:
        actual.atributos['tipo'] = TipoDatos.CUALQUIERA
    

  def __visitar_contenido(self, actual):
    """
    Contenido ::= delta Instrucción* omicron
    """
    for nodo in actual.nodos:
      nodo.visitar(self)

    actual.atributos['tipo'] = TipoDatos.NINGUNO 

    for nodo in actual.nodos:
      if nodo.atributos['tipo'] != TipoDatos.NINGUNO:
        actual.atributos['tipo'] = nodo.atributos['tipo']
  def manejoErrores(self, nodo, tipoError):
    print("ERROR: " + tipoError)
    print("Fila: " + str(nodo.contenido.fila) )
    print("Columna: " + str(nodo.contenido.columna) )
    print("Por favor, revisar el siguiente componente: " + str(nodo.contenido.string) ) 
    sys.exit(1)
       
