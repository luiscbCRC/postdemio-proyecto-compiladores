from Analizador.Arbol.arbol import ArbolSintaxisAbstracta, TipoNodo, NodoArbol
from Verificador.TablaSimbolos import TablaSimbolos
from Verificador.TipoDatos.tiposDatos import TipoDatos
from Verificador.Visitante import Visitante


class Verificador:

  asa           : ArbolSintaxisAbstracta
  visitador     : Visitante
  tablaSimbolos : TablaSimbolos

  def __init__(self, nuevo_asa: ArbolSintaxisAbstracta):

    self.asa = nuevo_asa
    self.tablaSimbolos = TablaSimbolos()
    self.__cargar_ambiente_estandar()
    self.visitador = Visitante(self.tablaSimbolos)


  def imprimir_asa(self):

    if self.asa.raiz is None:
      print([])
    else:
      self.asa.imprimir_preorden()


  def __cargar_ambiente_estandar(self):

    funcionesEstandar = [
      ('elMinistroSalasDijo', TipoDatos.NINGUNO),             #Revisar
      ('casosDeHoy', TipoDatos.NINGUNO),
      ('toser', TipoDatos.NINGUNO),
      ('dadoDeAlta', TipoDatos.NINGUNO),
      ('cantidadDeCasos', TipoDatos.NÚMERO),
      ('obtenerCaso', TipoDatos.TEXTO),
      ('vacuna', TipoDatos.CUALQUIERA)
    ]

    for nombre, tipo in  funcionesEstandar:
      nodo = NodoArbol(TipoNodo.FUNCION, contenido=nombre, atributos= {'tipo': tipo})
      self.tablaSimbolos.registrar_simbolo(nodo)


  def verificar(self):
    self.visitador.visitar(self.asa.raiz)

  def inicio(self):
    self.verificar()
