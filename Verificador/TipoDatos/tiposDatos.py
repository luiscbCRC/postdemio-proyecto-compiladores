from enum import Enum, auto

class TipoDatos(Enum):

    TEXTO        = auto()
    CUALQUIERA   = auto()
    NÚMERO       = auto()
    ENTERO       = auto()
    FLOTANTE     = auto()
    VALOR_VERDAD = auto()
    NINGUNO      = auto()
