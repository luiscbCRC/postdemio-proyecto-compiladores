# Este archivo es el punto de entradad de todo el programa
# from Analizador.main import Analizador
# Se importan bibliotecas para manejar los argumentos y el sistema
import argparse
import os
import sys

# Se importan las clases necesarias del explorador
from Archivo import CargarArchivo

from Explorador.main import Explorador
from Analizador.main import Analizador
from Verificador.main import Verificador
from Generador.main import Generador



class Postdemio:
  """
  Esta clase representa el programa completo para llamar a las distintas secciones del transpilador.

  ...

  Atributos
  ----------
  cargar_archivo : CargarArchivo
    Clase manejadora de todo lo referente a archivos
  ruta_archivo : str
    La ruta que maneja el programa para encontrar el código en Postdemio

  Métodos
  -------
  inicio():
    Punto de inicio del programa para instanciar y hacer llamados principales del flujo principal

  manejar_argumentos():
    Maneja los argumentos que son pasados por el usuario en la terminal

  buscar_archivo(ruta_archivo:string):
    Llama a los métodos de CargarArchivo para almacenar el código Postdemio en una lista
  """


  def __init__(self, ruta_principal = "Postdemio/"):
    """
    Constructor de la clase Postdemio para inicializar los atributos esenciales.

    Parámetros
    ----------
    Ninguno
    """
    self.args = self.manejar_argumentos()

    self.explorador = Explorador(self.buscar_archivo(ruta_principal + self.args.archivo))
    self.analizador = Analizador(self.explorador.obtener_componentes())
    self.analizador.inicio()
    self.verificador = Verificador(self.analizador.obtener_arbol())
    self.generador = Generador(self.analizador.obtener_arbol())

  def inicio(self):
    """
    Punto de inicio del programa para instanciar todas las clases y llamar a los métodos correspondientes.

    Parameters
    ----------
    Ninguno

    Retorno
    -------
    Ninguno. Sale del programa
    """


    print("Proyecto de Compiladores e Intérpretes - Lenguaje Postdemio\n")

    if self.args.todo:
      self.generador.inicio()
      os.system("python3 Postdemio/NoError/carreraCaracoles.py")

    elif self.args.explorar:
      self.explorador.inicio()

    elif self.args.analizar:
      self.analizador.imprimir()

    elif self.args.verificar:
      self.verificador.inicio()

    elif self.args.generador:
      self.generador.inicio()
      self.generador.imprimir()

    else:
      print("Por favor, verifique cuales son los parámetros disponibles.")

  def manejar_argumentos(self):
    """
    Lee y almacena todos los argumentos que se pasan por terminal para ejecutar las distintas secciones.

    Tambien almacena el nombre del archivo con el que se desea trabajar.

    Parámetros
    ----------
    Ninguno

    Returns
    -------
    Retorna un objeto con los argumentos que el usuario agregó.
    """

    argsParser = argparse.ArgumentParser()
    argsParser.add_argument("archivo")
    argsParser.add_argument('-E', '--explorar', help='Realiza la exploracion del archivo', action='store_true')
    argsParser.add_argument('-A', '--analizar', help='Realiza el analisis del archivo', action='store_true')
    argsParser.add_argument('-V', '--verificar', help='Realiza el verificador del archivo', action='store_true')
    argsParser.add_argument('-G', '--generador', help='Realiza el generador del archivo', action='store_true')
    argsParser.add_argument('-T', '--todo', help='Corre todas las etapas del transpilador', action='store_true')
    return argsParser.parse_args()

  def buscar_archivo(self, ruta_archivo):
    """
    Sirve para encontrar el archivo que el usuario especificó para trabajar.

    Si el archivo no es encontrado o sucede un error, se despliega un mensaje de error y se sale del programa.

    Parámtros
    ----------
    ruta_archivo : string, requerido
      Ruta completa relativa de donde se encuentra el archivo

    Retorno
    -------
    Una lista con todas las lineas de código del archivo
    """
    cargar_archivo = CargarArchivo()
    print(ruta_archivo)

    try:
      codigo = cargar_archivo.archivo_a_lista(ruta_archivo)
    except IOError:
      print("El archivo no fue encontrado. Por favor verifique que el nombre del archivo sea correcto y se encuentre en la carpeta Postdemio")
      sys.exit(1)
    except:
      print("Ocurrió un error insesperado. Por favor contacte al administrador del programa.")
      sys.exit(1)
    return codigo

if __name__ == '__main__':
  postdemio = Postdemio()
  postdemio.inicio()
