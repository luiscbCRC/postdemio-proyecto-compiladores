from Explorador.Componente import Componente
from Analizador.Arbol.arbol import ArbolSintaxisAbstracta, TipoNodo, NodoArbol # Implementacion dudosa - Revisar


class AnalizadorCompleto:
  """
  La clase AnalizadorCompleto maneja toda la funcionalidad final para crear el
  Arbol de Sintaxis Abstracta como parte del analizador del programa.

  ...

  Atributos
  ----------
  componentes : lista
    La lista de todos los componentes que se reciben del explorador
  componente_actual : Componente
    El componente actual que se está analizando
  posicionActual : int
    Posición actual en la lista de componentes disponibles
  arbol_sintaxis_abstracta : ArbolSintaxisAbstracta()
    Objeto que contiene las funcionalidades del ASA

  Métodos
  ----------
  Cada método en su inicialización tiene la descripción correspondiente para cada caso.
  """

  def __init__(self, lista_componentes):
    """
    Constructor necesario para inicializar variables esenciales del analizador.

    Parámetros
    ----------
    lista_componentes: lista de componentes léxicos que se van a analizar
    """
    self.componentes = lista_componentes
    self.componente_actual = lista_componentes[0]
    self.posicionActual = 0
    self.arbol_sintaxis_abstracta = ArbolSintaxisAbstracta()
    
  def analizar(self):
    """
    Punto de entrada sin parámetros para llamar a las funciones privadas de la clase.
    """
    self.arbol_sintaxis_abstracta.raiz = self.__analizar_programa()
    return self.arbol_sintaxis_abstracta

  def __analizar_programa(self, nodos_nuevos = []):
    """
    Programa ::= (Asignación | Invocaciones | Comentario | Función)* covid Contenido
    """
    # Asignación
    if self.componente_actual.tipo == Componente.ASIGNACION:
      nodos_nuevos += [self.__analizar_asignacion()]
      self.__analizar_programa(nodos_nuevos)

    # Invocacion
    elif self.componente_actual.string == "ejecutar":
      nodos_nuevos += [self.__analizar_invocaciones()]
      self.__analizar_programa(nodos_nuevos)

    # Función
    elif self.componente_actual.string == "pandemia":
      nodos_nuevos += [self.__analizar_funcion()]
      self.__analizar_programa(nodos_nuevos)

    # Parte final del programa
    else:
      nodos_nuevos += [self.__analizar_principal()]
    
    # A este punto estamos retornando el árbol completo 
    return NodoArbol(TipoNodo.PROGRAMA, nodos=nodos_nuevos)

  def __analizar_principal(self):
    """
    Funcion que analiza la funcion principal y retorna el nodo creado respectivo
    """
    self.__analizar_palabra_clave("covid")
    nodos_nuevos = [self.__analizar_contenido()]
    return NodoArbol(TipoNodo.PRINCIPAL, nodos = nodos_nuevos)

  def __analizar_palabra_clave(self, palabra):
    # Esta no esta dentro de la gramática, pero funciona para no repetir código
    self.__verificar(palabra)

  def __analizar_instruccion(self):
    """
    Instrucción ::= (Comentario | Asignación | Lógica | Invocaciones)
    """
    nodos_nuevos = []
    # Asignacion
    if self.componente_actual.tipo == Componente.ASIGNACION:
      nodos_nuevos += [self.__analizar_asignacion()]
    # Iteracion
    elif self.componente_actual.tipo == Componente.ITERACION:
      nodos_nuevos += [self.__analizar_iteracion()]
    # Condicional
    elif self.componente_actual.tipo == Componente.CONDICIONAL:
      nodos_nuevos += [self.__analizar_condicion()]
    # Invocacion
    elif self.componente_actual.string == "ejecutar":
      nodos_nuevos += [self.__analizar_invocaciones()]
    # Retorno
    else:
      nodos_nuevos += [self.__analizar_retorno()]

    return NodoArbol(TipoNodo.INSTRUCCION, nodos = nodos_nuevos)

  def __analizar_invocaciones(self):
    """
    Invocaciones ::= (Parámetro?) Identificador ejecutar
    """
    nodos_nuevos = []
    self.__verificar("ejecutar")
    # Identificador
    if self.componente_actual.tipo == Componente.IDENTIFICADOR:
      nodos_nuevos += [self.__verificar_identificador()]
    # Funciones Estandar
    else:
      nodos_nuevos += [self.__verificar_funciones_estandar()]

    self.__verificar(")")
    nodos_nuevos += [self.__analizar_parametro()]
    self.__verificar("(")
    return NodoArbol(TipoNodo.INVOCACION, nodos = nodos_nuevos)

  def __analizar_asignacion(self):
    """
    Asignación ::= (Operación | Valor | Identificador | Invocaciones), Identificador virus
    """
    nodos_nuevos = []

    self.__verificar("virus")
    nodos_nuevos += [self.__verificar_identificador()]
    self.__verificar(",")

    # Separadores
    if self.componente_actual.tipo == Componente.SEPARADORES:
      nodos_nuevos += [self.__analizar_operacion()]

    # Valores
    elif self.componente_actual.tipo in [Componente.ENTERO, Componente.FLOTANTE, Componente.CADENA_CARACTERES]:
      nodos_nuevos += [self.__analizar_valor()]

    # Identificador
    elif self.componente_actual.tipo == Componente.IDENTIFICADOR:
      nodos_nuevos += [self.__verificar_identificador()]

    # Invocaciones
    else:
      nodos_nuevos += [self.__analizar_invocaciones()]

    # Nodo creado con sus nodos hijos
    return NodoArbol(TipoNodo.ASIGNACION, nodos = nodos_nuevos)
  

  def __verificar_funciones_estandar(self):
    """
    Funcion que verifica las funciones estandar y retorna el nodo creado respectivo
    """
    nodo = NodoArbol(TipoNodo.AMBIENTE_ESTANDAR, contenido = self.componente_actual)
    self.__pasarComponente()
    return nodo

  def __analizar_valor(self):
    """
    Funcion que analiza si el compoenente es un numero o una cadena de caracteres,
    y retorna el nodo respectivo
    """
    # Se revisa si el Componente es una cadena de caracteres
    if self.componente_actual.tipo is Componente.CADENA_CARACTERES:
      nodo = self.__verificar_texto()

    # Es número
    else:
      nodo = self.__analizar_numero()

    return nodo

  def __analizar_numero(self):
    # Verifica si es un numero entero
    if self.componente_actual.tipo == Componente.ENTERO:
      nodo = self.__verificar_entero()
    # Es flotante
    else:
      nodo = self.__verificar_flotante()

    return nodo

  def __verificar_texto(self):
    """
    Funcion que verifica el texto y retorna el nodo creado respectivo
    """
    nodo = NodoArbol(TipoNodo.TEXTO, contenido =self.componente_actual)
    self.__pasarComponente()
    return nodo


  def __verificar_entero(self):
    """
    Funcion que verifica los numeros enteros y retorna el nodo creado respectivo
    """
    nodo = NodoArbol(TipoNodo.ENTERO, contenido =self.componente_actual)
    self.__pasarComponente()
    return nodo


  def __verificar_flotante(self):
    """
    Funcion que verifica los numeros flotantes y retorna el nodo creado respectivo
    """
    nodo = NodoArbol(TipoNodo.FLOTANTE, contenido =self.componente_actual)
    self.__pasarComponente()
    return nodo

  def __analizar_contenido(self):
    '''
    Contenido ::= delta Instrucción* omicron
    '''
    nodos_nuevos = []

    self.__analizar_palabra_clave("delta")
    while self.componente_actual.string != "omicron":
      nodos_nuevos += [self.__analizar_instruccion()]
    self.__analizar_palabra_clave("omicron")

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.CONTENIDO, nodos = nodos_nuevos)

  def __analizar_comparacion(self):
    '''
    Comparación ::= {Operando Operando Comparador}
    '''
    nodos_nuevos = []
    
    self.__verificar("}")

    # Verifica cada uno de los componentes de la comparacion
    nodo2 = self.__verificar_comparador()    
    nodo3 = self.__verificar_operando()
    nodo1 = self.__verificar_operando()

    # Agrega los nodos a nuevos_nodos
    nodos_nuevos += [nodo1]
    nodos_nuevos += [nodo2]
    nodos_nuevos += [nodo3]

    self.__verificar('{')

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.COMPARACION, nodos=nodos_nuevos)

  def __analizar_operacion(self):
    '''
    Operación ::= [Operando Operando Operador]
    '''
    nodos_nuevos = []
    self.__verificar("]")

    # Verifica cada uno de los componentes de la operacion
    nodo2 = self.__verificar_operador()
    nodo3 = self.__verificar_operando()
    nodo1= self.__verificar_operando()

    # Agrega los nodos a nuevos_nodos
    nodos_nuevos += [nodo1]
    nodos_nuevos += [nodo2]
    nodos_nuevos += [nodo3]
    
    self.__verificar('[')
    #Retorna el nodo respectivo
    return NodoArbol(TipoNodo.OPERACION, nodos = nodos_nuevos)


  def __analizar_condicion(self):
    """
    Condición ::= Si (Sino)?
    """
    nodos_nuevos = []
    # Se analiza el Si obligatorio
    nodos_nuevos += [self.__analizar_Si()]

    # El Sino es opcional
    if self.componente_actual.string == 'curado':
      nodos_nuevos += [self.__analizar_Sino()]

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.CONDICION, nodos = nodos_nuevos)


  def __analizar_Si(self):
    """
    Si ::= Comparacion contagio Contenido
    """
    nodos_nuevos = []
    self.__verificar('contagio')
    nodos_nuevos += [self.__analizar_comparacion()]
    nodos_nuevos += [self.__analizar_contenido()]

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.SI, nodos = nodos_nuevos)

  def __analizar_Sino(self):
    """
    Sino ::= curado Contenido
    """
    self.__verificar('curado')
    nodos_nuevos = [self.__analizar_contenido()]

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.SINO, nodos = nodos_nuevos)

  def __analizar_iteracion(self):
    """
    Iteracion ::= cuarentena Contenido
    """
    nodos_nuevos = []
    self.__verificar('cuarentena')
    nodos_nuevos += [self.__analizar_comparacion()]
    nodos_nuevos += [self.__analizar_contenido()]

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.ITERACION, nodos = nodos_nuevos)


  def __verificar(self, texto_esperado):
    """
    Verifica si el texto del componente léxico actual corresponde con
    el esperado cómo argumento
    """
    if self.componente_actual.string == texto_esperado:
      self.__pasarComponente()

  def __pasarComponente(self):
    """
    Pasa al siguiente componente léxico
    """
    self.posicionActual += 1
    if self.posicionActual >= len(self.componentes):
      return

    self.componente_actual = self.componentes[self.posicionActual]

  def __verificar_identificador(self):
    """
    Funcion que verifica los identficadores, crea el nodo respectivo y lo retorna
    """
    nodo = NodoArbol(TipoNodo.IDENTIFICADOR, contenido =self.componente_actual)
    self.__pasarComponente()
    return nodo

  def __analizar_referencia(self):
    """
    Referencia ::= Identificador | Identificador, Referencia 
    """
    nodos_nuevos = []
    if self.componente_actual.tipo is not Componente.SEPARADORES:
      # Itera por cada una de las referencias que existen
      while self.componente_actual.tipo is not Componente.SEPARADORES:
        nodos_nuevos += [self.__verificar_identificador()]

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.REFERENCIAS, nodos = nodos_nuevos)

  def __analizar_parametro(self):
    """
    Parametro ::= Variable | Variable, Parámetro
    """
    nodos_nuevos = []
    if self.componente_actual.tipo is not Componente.SEPARADORES:
      # Itera por cada uno de los parametros
      while self.componente_actual.tipo is not Componente.SEPARADORES:
        nodos_nuevos += [self.__verificar_operando()]

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.PARAMETROS, nodos = nodos_nuevos)

  def __analizar_variable(self):
    """
    Variable ::= (Valor | Identificador)
    """
    # Verifica si es un valor
    if self.componente_actual.tipo in [Componente.ENTERO, Componente.FLOTANTE, Componente.CADENA_CARACTERES]:
      nodos_nuevos = [self.__analizar_valor()]
    # Es identificador
    else:
      nodos_nuevos = [self.__verificar_identificador()]

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.VARIABLE, nodos=nodos_nuevos)

  def __analizar_retorno(self):
    """
    Retorno ::= Variable toser
    """
    self.__analizar_palabra_clave("toser")
    nodos_nuevos = [self.__analizar_variable()]

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.RETORNO, nodos=nodos_nuevos)

  def __analizar_funcion(self):
    """
    Función :== (Referencia?) Identificador pandemia delta Instrucción* Retorno omicron
    """
    nodos_nuevos = []
    self.__analizar_palabra_clave("pandemia")
    # Identificador
    if self.componente_actual.tipo == Componente.IDENTIFICADOR:
      nodos_nuevos += [self.__verificar_identificador()]
      self.__verificar(')')
      nodos_nuevos += [self.__analizar_referencia()]
      self.__verificar('(')
    self.__analizar_palabra_clave("delta")
    # Itera por cada una de las instrucciones
    while self.componente_actual.string != "omicron":
      if self.componente_actual.string == "toser":
        nodos_nuevos += [self.__analizar_retorno()]
      else:
        nodos_nuevos += [self.__analizar_instruccion()]
    self.__analizar_palabra_clave("omicron")

    # Retorna el nodo respectivo
    return NodoArbol(TipoNodo.FUNCION, nodos=nodos_nuevos)


  def __verificar_comparador(self):
    """
    Funcion que verifica los comparadores, crea el nodo respectivo y lo retorna
    """
    nodo = NodoArbol(TipoNodo.COMPARADOR, contenido =self.componente_actual)
    self.__pasarComponente()
    return nodo


  def __verificar_operador(self):
    """
    Funcion que verifica los operadores, crea el nodo respectivo y lo retorna
    """
    nodo = NodoArbol(TipoNodo.OPERADOR, contenido =self.componente_actual)
    self.__pasarComponente()
    return nodo


  def __verificar_operando(self):
    """
    Funcion que verifica los operandos, crea el nodo respectivo y lo retorna
    """
    if self.componente_actual.tipo in [Componente.ENTERO, Componente.FLOTANTE, Componente.CADENA_CARACTERES]:
      nodos_nuevos = [self.__analizar_valor()]
    else:
      nodos_nuevos = [self.__verificar_identificador()]
    return NodoArbol(TipoNodo.OPERANDO, nodos=nodos_nuevos)
