# Clases para el manejo de un árbol de sintáxis abstracta

from enum import Enum, auto
from Explorador.Componente import Componente 
import copy

class TipoNodo(Enum):
  """
  Describe el tipo de nodo del árbol
  """
  PROGRAMA              = auto()
  ASIGNACION            = auto()
  IDENTIFICADOR         = auto()
  ENTERO                = auto()
  FLOTANTE              = auto()
  TEXTO                 = auto()
  INVOCACION            = auto()
  PARAMETROS = auto()
  REFERENCIAS    = auto()
  EXPRESION_MATEMATICA  = auto()
  OPERACION             = auto()
  OPERADOR              = auto()
  FUNCION               = auto()
  INSTRUCCION           = auto()
  CONDICION           = auto()
  SI              = auto()
  SINO                  = auto()
  COMPARACION           = auto()
  COMPARADOR            = auto()
  RETORNO               = auto()
  ITERACION            = auto()
  PRINCIPAL             = auto()
  BLOQUE_INSTRUCCIONES  = auto()
  OPERADOR_LOGICO       = auto()
  AMBIENTE_ESTANDAR    = auto()
  OPERANDO = auto()
  VARIABLE = auto()
  CONTENIDO = auto()

class NodoArbol:
  """
  Define los atributos que va a tener cada nodo del árbol
  """
  tipo      : TipoNodo
  contenido : Componente
  atributos : dict
  nodos     : list

  """
  Constructor para crear objetos de tipo NodoArbol
  """
  def __init__(self, tipo, contenido = None, nodos = [], atributos = {}):
    self.tipo      = tipo
    self.contenido = contenido
    self.nodos     = nodos
    self.atributos = copy.deepcopy(atributos)

  def visitar(self, visitador):
    return visitador.visitar(self)


  
class ArbolSintaxisAbstracta:
  """
  Clase que tiene la raiz del ASA y los metodos de impresion del arbol
  """

  raiz : NodoArbol
  
  def __init__(self):
    self.raiz = None
  

  """
  Imprime el arbol en preorden
  """

  def imprimir_preorden(self):
    self.__preorden(self.raiz, 0)

  """
  Funcion auxiliar que se encarga de imprimir el arbol con la estructura deseada
  """
  def __preorden(self, nodo, tabs):
    if nodo is not None:
      for i in range(tabs):
        print("\t", end="")
      if nodo.contenido is not None:
        print(nodo.tipo, "(", nodo.contenido.string, ")")
      else:
        print(nodo.tipo)
      for nodo in nodo.nodos:
        self.__preorden(nodo, tabs+1)