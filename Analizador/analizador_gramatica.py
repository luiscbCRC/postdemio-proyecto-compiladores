from Explorador.Componente import Componente
import sys


class AnalizadorGramatica:
  """
  La clase AnalizadorGramatica maneja toda la funcionalidad de la seccion de manejo de errores
  del analizador.

  ...

  Atributos
  ----------
  componentes : lista
    La lista de todos los componentes que se reciben del explorador
  componente_actual : Componente
    El componente actual que se está analizando
  instruccionAnalizadaActual : string
    La instrucción actual que se está trabajando para hacer un mejor despliegue de errores.
  posicionActual : int
    Posición actual en la lista de componentes disponibles

  Métodos
  ----------
  Cada método en su inicialización tiene la descripción correspondiente para cada caso.
  Ningún método retorna nada, puesto que se trabaja con un objeto de componentes que ya viene del explorador.
  """

  def __init__(self, lista_componentes):
    """
    Constructor necesario para inicializar variables esenciales del explorador.

    Parámetros
    ----------
    lista_componentes: lista de componentes léxicos que se van a analizar
    """
    self.componentes = lista_componentes
    self.componente_actual = lista_componentes[0]
    self.instruccionAnalizadaActual = ""
    self.posicionActual = 0
    
  def analizar(self):
    """
    Punto de entrada sin parámetros ni return para llamar a las funciones privadas de la clase.
    """
    self.__analizar_programa()

  def __analizar_programa(self):
    """
    Programa ::= (Asignación | Invocaciones | Comentario | Lógica | Función)* covid Contenido
    """
    self.instruccionAnalizadaActual = 'Programa'
    # Asignación
    if self.componente_actual.tipo == Componente.ASIGNACION:
      self.__analizar_asignacion()
      self.__analizar_programa()

    # Invocacion
    elif self.componente_actual.string == "ejecutar":
      self.__analizar_invocaciones()
      self.__analizar_programa()

    # Lógica
    elif self.componente_actual.tipo == Componente.OPERADOR:
      self.__analizar_logica()
      self.__analizar_programa()

    # Función
    elif self.componente_actual.string == "pandemia":
      self.__analizar_funcion()
      self.__analizar_programa()

    # Parte final del programa
    elif self.componente_actual.tipo == Componente.PALABRA_CLAVE:
      self.__analizar_palabra_clave("covid")
      self.__analizar_contenido()
      # Se supone que si llega aqui, entonces el programa ya debería termina.

    # Si se llega a este punto, el programa no pudo identificar ningún punto de entrada. Error general, no se puede continuar.
    else:
      self.manejarErrores("Instruccion no soportada en este bloque")

  def __analizar_palabra_clave(self, palabra):
    self.instruccionAnalizadaActual = 'Palabra Clave'
    # Esta no esta dentro de la gramática, pero funciona para no repetir código
    if self.componente_actual.tipo is Componente.PALABRA_CLAVE:
      self.__verificar(palabra)
    else:
      self.manejarErrores("Se esperaba: \"" + palabra + "\"" + ", Se obtuvo: \"" + self.componente_actual.string + "\"")

  def __analizar_instruccion(self):
    """
    Instrucción ::= (Comentario | Asignación | Lógica | Invocaciones)
    """
    self.instruccionAnalizadaActual = 'Instruccion'
    if self.componente_actual.tipo == Componente.ASIGNACION:
      self.__analizar_asignacion()
    elif self.componente_actual.tipo == Componente.ITERACION:
      self.__analizar_iteracion()
    elif self.componente_actual.tipo == Componente.CONDICIONAL:
      self.__analizar_condicion()
    elif self.componente_actual.string == "ejecutar":
      self.__analizar_invocaciones()
    elif self.componente_actual.string == "toser":
      self.__analizar_retorno()
    else:
      self.manejarErrores("Mala definicion de la instruccion")

  def __analizar_invocaciones(self):
    """
    Invocaciones ::= (Parámetro?) Identificador ejecutar
    """
    self.instruccionAnalizadaActual = 'Invocacion'
    self.__verificar("ejecutar")
    if self.componente_actual.tipo == Componente.IDENTIFICADOR:
      self.__verificar_identificador()
    elif self.componente_actual.tipo == Componente.FUNCIONES_ESTANDAR:
      self.__pasarComponente()
    else:
      self.manejarErrores("No hay una funcion definida para ejecutar")
    self.__verificar(")")
    self.__analizar_parametro()
    self.__verificar("(")

  def __analizar_asignacion(self):
    """
    Asignación ::= (Operación | Valor | Identificador | Invocaciones), Identificador virus
    """
    self.instruccionAnalizadaActual = 'Asignacion'
    self.__verificar("virus")
    self.__verificar_identificador()
    self.__verificar(",")

    if self.componente_actual.tipo == Componente.SEPARADORES:
      self.__analizar_operacion()
    elif self.componente_actual.tipo in [Componente.ENTERO, Componente.FLOTANTE, Componente.CADENA_CARACTERES]:
      self.__pasarComponente()
    elif self.componente_actual.tipo == Componente.IDENTIFICADOR:
      self.__verificar_identificador()
    elif self.componente_actual.tipo == Componente.PALABRA_CLAVE:
      self.__analizar_invocaciones()
    else:
      self.manejarErrores("Mala defincion de la asignacion")

  def __analizar_contenido(self):
    '''
    Contenido ::= delta Instrucción* omicron
    '''
    # Despues de delta se asume que es una de estas instrucciones
    self.instruccionAnalizadaActual = 'ContenidoInstrucciones'
    self.__analizar_palabra_clave("delta")
    while self.componente_actual.string != "omicron":
      self.__analizar_instruccion()
    self.__analizar_palabra_clave("omicron")

  def __analizar_comparacion(self):
    '''
    Comparación ::= {Operando Operando Comparador}
    '''
    self.instruccionAnalizadaActual = 'Comparacion'
    
    self.__verificar("}")
    self.__verificar_comparador()    
    self.__verificar_operando()
    
    self.__verificar_operando()
    self.__verificar('{')

  def __analizar_operacion(self):
    '''
    Operación ::= [Operando Operando Operador]
    '''
    self.instruccionAnalizadaActual = 'Operacion'
    self.__verificar("]")
    self.__verificar_operador()
    self.__verificar_operando()
    self.__verificar_operando()
    self.__verificar('[')

  def __analizar_logica(self):
    """
    Lógica ::= Comparacion (Condición | Iteracion)
    """
    self.instruccionAnalizadaActual = 'Logica'
    if self.componente_actual.tipo == Componente.PALABRA_CLAVE:
      if self.componente_actual.string == 'cuarentena':
        self.__analizar_iteracion()  # self.__verificar('cuarentena')
        self.__analizar_comparacion()
      elif self.componente_actual.string == 'contagio':
        self.__analizar_condicion()  # self.__verificar(contagio)
        self.__analizar_comparacion()
      else:
        self.manejarErrores("Verifica que estas llamando correctamente a una condicion o iteracion, intenta con cuarentena o contagio")
    else:
      self.manejarErrores("Comprueba que estas utilizando una palabra clave del lenguaje para manejar una comparacion")

  def __analizar_condicion(self):
    """
    Condición ::= Si Sino?
    """
    self.instruccionAnalizadaActual = 'Condicion'
    # Se analiza el Si obligatorio
    self.__analizar_Si()

    # El Sino es opcional
    if self.componente_actual.string == 'curado':
      self.__analizar_Sino()

  def __analizar_Si(self):
    """
    Si ::= Comparacion contagio Contenido
    """
    self.instruccionAnalizadaActual = 'Si'
    self.__verificar('contagio')
    self.__analizar_comparacion()
    self.__analizar_contenido()

  def __analizar_Sino(self):
    """
    Sino ::= curado Contenido
    """
    self.instruccionAnalizadaActual = 'Sino'
    self.__verificar('curado')
    self.__analizar_contenido()

  def __analizar_iteracion(self):
    """
    Iteracion ::= cuarentena Contenido
    """
    self.instruccionAnalizadaActual = 'Iteracion'
    self.__verificar('cuarentena')
    self.__analizar_comparacion()
    self.__analizar_contenido()


  def manejarErrores(self, msj="No hay sugerencias para este error"):
    """
    Método que se encarga de manejar el reporte de errores
    """          
    instruccion = self.instruccionAnalizadaActual
    texto = self.componente_actual.string
    tipo = self.componente_actual.tipo.name
    fila = self.componente_actual.fila
    columna = self.componente_actual.columna
    error = f'>> Error en la instruccion: {instruccion} \nTexto: {texto} \nTipo: {tipo}  \nLinea: {fila} \nColumna: {columna}\nDescripcion: {msj}'
    print("Se ha encontrado un error en la sección del analizador.\n\n")
    print(error)
    sys.exit(1)

  def __verificar(self, texto_esperado):
    """
    Verifica si el texto del componente léxico actual corresponde con
    el esperado cómo argumento
    """
    if self.componente_actual.string != texto_esperado:
      self.manejarErrores("Se esperaba: \"" + texto_esperado + "\"" + ", Se obtuvo: \"" + self.componente_actual.string + "\"")
    else:
      self.__pasarComponente()

  def __pasarComponente(self):
    """
    Pasa al siguiente componente léxico
    """
    self.posicionActual += 1
    if self.posicionActual >= len(self.componentes):
      return

    self.componente_actual = self.componentes[self.posicionActual]

  def __verificar_identificador(self):
    self.instruccionAnalizadaActual = 'Identificador'
    if self.componente_actual.tipo == Componente.IDENTIFICADOR:
      self.__pasarComponente()
    else:
      self.manejarErrores("No es un identificador valido")

  def __analizar_referencia(self):
    """
    Referencia ::= Identificador | Identificador, Referencia 
    """
    self.instruccionAnalizadaActual = 'Referencia'
    if self.componente_actual.tipo is not Componente.SEPARADORES:
      while self.componente_actual.tipo is not Componente.SEPARADORES:
        self.__verificar_identificador()


  def __analizar_parametro(self):
    """
    Parametro ::= Variable | Variable, Parámetro
    """
    self.instruccionAnalizadaActual = 'Parametro'
    if self.componente_actual.tipo is not Componente.SEPARADORES:
      while self.componente_actual.tipo is not Componente.SEPARADORES:
        self.__verificar_operando()

  def __analizar_variable(self):
    """
    Variable ::= (Valor | Identificador)
    """
    self.instruccionAnalizadaActual = 'Variable'
    if self.componente_actual.tipo in [Componente.ENTERO, Componente.FLOTANTE, Componente.CADENA_CARACTERES]:
      self.__pasarComponente()
    elif self.componente_actual.tipo == Componente.IDENTIFICADOR:
      self.__verificar_identificador()
    else:
      self.manejarErrores("El componente no es de tipo valor o identificador")

  def __analizar_retorno(self):
    """
    Retorno ::= Variable toser
    """
    self.instruccionAnalizadaActual = 'Retorno'
    self.__analizar_palabra_clave("toser")
    self.__analizar_variable()

  def __analizar_funcion(self):
    """
    Función :== (Referencia?) Identificador pandemia delta Instrucción* Retorno omicron
    """
    self.instruccionAnalizadaActual = 'Funcion'
    self.__analizar_palabra_clave("pandemia")
    if self.componente_actual.tipo == Componente.IDENTIFICADOR:
      self.__verificar_identificador()
      self.__verificar(')')
      self.__analizar_referencia()
      self.__verificar('(')
    self.__analizar_palabra_clave("delta")
    while self.componente_actual.string != "omicron":
      if self.componente_actual.string == "toser":
        self.__analizar_retorno()
      else:
        self.__analizar_instruccion()
    self.__analizar_palabra_clave("omicron")

  # Las siguientes funciones sirven únicamente para verificar según sea el caso, que el tipo de componente que se está
  # buscando, efectivamente cumple con el tipo que se solicita. Funciona por el momento para manejar errores personalizados.
  def __verificar_comparador(self):
    self.instruccionAnalizadaActual = 'Comparador'
    if self.componente_actual.tipo == Componente.COMPARADOR:
      self.__pasarComponente()
    else:
      self.manejarErrores("El operador para comparar no es valido")

  def __verificar_operador(self):
    self.instruccionAnalizadaActual = 'Operador'
    if self.componente_actual.tipo == Componente.OPERADOR:
      self.__pasarComponente()
    else:
      self.manejarErrores("El operador de la operacion no es valido")

  def __verificar_operando(self):
    self.instruccionAnalizadaActual = 'Operando'
    if self.componente_actual.tipo in [Componente.ENTERO, Componente.FLOTANTE, Componente.CADENA_CARACTERES]:
      self.__pasarComponente()
    elif self.componente_actual.tipo == Componente.IDENTIFICADOR:
      self.__verificar_identificador()
    else:
      self.manejarErrores("El operando no es un valor ni un identificador")


  def __verificar_texto(self):
    self.instruccionAnalizadaActual = 'texto'
    if self.componente_actual.tipo == Componente.CADENA_CARACTERES:
      self.__pasarComponente()
    else:
      self.manejarErrores()
      
      
      
  def __verificar_caracter(self):
    self.instruccionAnalizadaActual = 'caracter'
    if self.componente_actual.tipo == Componente.CARACTERES:
      self.__pasarComponente()
    else:
      self.manejarErrores()
      
      
  def __analizar_numero(self):
    """
    Numero ::= (Entero | Flotante)
    """
    self.instruccionAnalizadaActual = 'Numero'
    self.__verificar_identificador()

    if self.componente_actual.tipo == Componente.ENTERO or Componente.FLOTANTE:
      self.__pasarComponente()
    else:
      self.manejarErrores()
