from Analizador.analizador_gramatica import AnalizadorGramatica
from Analizador.analizador_completo import AnalizadorCompleto

class Analizador:
  """
  En esta clase se encuentra la implementación inicial de los llamados públicos del analizador.
  A partir de aquí, se importan todas las clases y estructuras de control necesarias para trabajar el analizador.
  """


  def __init__(self, lista_componentes):
    """
    Constructor necesario para inicializar variables esenciales del explorador.

    Parámetros
    ----------
    lista_componentes: lista de componentes léxicos que se van a analizar
    """
    self.analizador_gramatica = AnalizadorGramatica(lista_componentes)
    self.analizador_completo = AnalizadorCompleto(lista_componentes)
    self.arbol = None  


  def inicio(self, imprimir = False):
    """
    Punto de entrada para ejecutar todas las funcionalidades y dependencias del analizador.

    Parámetros
    ----------
    Ninguno

    Retorno
    -------
    Por el momento, no tiene retorno, pero en el futuro, se tendrá una estructura de árbol
    """
    
    self.analizador_gramatica.analizar()
    self.arbol = self.analizador_completo.analizar()

  def imprimir(self):
    self.arbol.imprimir_preorden()
    print("Ha concluído la etapa del Explorador y el Analizador con éxito.\n")

  def obtener_arbol(self):
    return self.arbol
