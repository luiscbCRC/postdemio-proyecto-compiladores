import os

from Analizador.Arbol.arbol import ArbolSintaxisAbstracta
from Generador.Visitador import Visitador

class Generador:

  asa : ArbolSintaxisAbstracta
  visitador : Visitador
  ambiente_estandar = """import sys
import random as rand
def elMinistroSalasDijo(texto):
\tprint(texto)
def casosDeHoy(min, max):
\treturn rand.randint(min, max)
def vacuna(string):
\treturn input(string)
def cantidadDeCasos(string):
\treturn len(string)
def obtenerCaso(string, entero):
\treturn string[entero]\n\n"""

  def __init__(self, asa: ArbolSintaxisAbstracta):
    self.asa = asa
    self.visitador = Visitador()
    self.resultado = ""

  def inicio(self):
    self.resultado = self.ambiente_estandar + self.visitador.visitar(self.asa.raiz)
    file = open("Postdemio/NoError/carreraCaracoles.py", "w")
    n = file.write(self.resultado)
    file.close()

  def imprimir(self):
    print(self.resultado)
