from Analizador.Arbol.arbol import TipoNodo
from Analizador.Arbol.arbol import NodoArbol

class Visitador:

    tabuladores = 0

    def visitar(self, nodo : NodoArbol):
        """
        Este método es necesario por que uso un solo tipo de nodo para
        todas las partes del árbol por facilidad... pero cómo lo hice
        tuanis allá... pues bueno... acá hay que pagar el costo.
        """

        resultado = ''

        if nodo.tipo is TipoNodo.PROGRAMA:
            resultado = self.__visitar_programa(nodo)

        elif nodo.tipo is TipoNodo.IDENTIFICADOR:
            resultado = self.__visitar_identificador(nodo)

        elif nodo.tipo is TipoNodo.ASIGNACION:
            resultado = self.__visitar_asignacion(nodo)

        elif nodo.tipo is TipoNodo.ENTERO:
            resultado = self.__visitar_entero(nodo)

        elif nodo.tipo is TipoNodo.FLOTANTE:
            resultado = self.__visitar_flotante(nodo)

        elif nodo.tipo is TipoNodo.INVOCACION:
            resultado = self.__visitar_invocacion(nodo)

        elif nodo.tipo is TipoNodo.PARAMETROS:
            resultado = self.__visitar_parametros(nodo)

        elif nodo.tipo is TipoNodo.REFERENCIAS:
            resultado = self.__visitar_referencia(nodo)

        elif nodo.tipo is TipoNodo.EXPRESION_MATEMATICA:
            resultado = self.__visitar_expresion_matematica(nodo)

        elif nodo.tipo is TipoNodo.OPERACION:
            resultado = self.__visitar_operacion(nodo)

        elif nodo.tipo is TipoNodo.OPERADOR:
            resultado = self.__visitar_operador(nodo)

        elif nodo.tipo is TipoNodo.FUNCION:
            resultado = self.__visitar_funcion(nodo)

        elif nodo.tipo is TipoNodo.INSTRUCCION:
            resultado = self.__visitar_instruccion(nodo)

        elif nodo.tipo is TipoNodo.CONDICION:
            resultado = self.__visitar_condicion(nodo)

# ----------------------------------------------------------------- #

        elif nodo.tipo is TipoNodo.SI:
            resultado = self.__visitar_si(nodo)

        elif nodo.tipo is TipoNodo.SINO:
            resultado = self.__visitar_sino(nodo)

        elif nodo.tipo is TipoNodo.COMPARACION:
            resultado = self.__visitar_comparacion(nodo)

        elif nodo.tipo is TipoNodo.COMPARADOR:
            resultado = self.__visitar_comparador(nodo)

        elif nodo.tipo is TipoNodo.RETORNO:
            resultado = self.__visitar_retorno(nodo)

        elif nodo.tipo is TipoNodo.ITERACION:
            resultado = self.__visitar__iteracion(nodo)

        elif nodo.tipo is TipoNodo.PRINCIPAL:
            resultado = self.__visitar_principal(nodo)

        elif nodo.tipo is TipoNodo.AMBIENTE_ESTANDAR:
            resultado = self.__visitar_ambiente_estandar(nodo)

        elif nodo.tipo is TipoNodo.OPERANDO:
            resultado = self.__visitar_operando(nodo)

        elif nodo.tipo is TipoNodo.VARIABLE:
            resultado = self.__visitar_variable(nodo)

        elif nodo.tipo is TipoNodo.CONTENIDO:
            resultado = self.__visitar_contenido(nodo)

        elif nodo.tipo is TipoNodo.TEXTO:
            resultado = nodo.contenido.string

        return resultado

    def __visitar_programa(self, nodo_actual):
        """
        Programa ::= (Asignación | Invocaciones | Comentario | Lógica)* covid Contenido
        """
        resultado = ""

        for nodo in nodo_actual.nodos:
            resultado += self.visitar(nodo) + "\n"

        return resultado

    def __visitar_asignacion(self, nodo_actual):
        """
        Asignación ::= (Operación | Valor | Identificador | Invocaciones), Identificador virus
        """
        resultado = """{} = {}"""

        return resultado.format(self.visitar(nodo_actual.nodos[0]),
                                self.visitar(nodo_actual.nodos[1]))

    def __visitar_entero(self, nodo_actual):
        """
        Entero ::= [-+]?[0-9]+
        """
        return nodo_actual.contenido.string

    def __visitar_flotante(self, nodo_actual):
        """
        Flotante ::= [-+]?([0-9]+)?\.[0-9]+
        """
        return nodo_actual.contenido.string

    def __visitar_invocacion(self, nodo_actual):
        """
        Invocaciones ::= ejecutar (Parámetro?) Identificador
        """
        resultado = """{}({})"""

        return resultado.format(self.visitar(nodo_actual.nodos[0]),
                                self.visitar(nodo_actual.nodos[1]))

    def __visitar_parametros(self, nodo_actual):
        parametros = []

        for nodo in nodo_actual.nodos:
            parametros.append(self.visitar(nodo))

        if len(parametros) > 0:
            return ", ".join(parametros)
        else:
            return ""

    def __visitar_referencia(self, nodo_actual):
        """
        Referencia ::= Identificador | Identificador, Referencia
        """
        referencias = []

        for nodo in nodo_actual.nodos:
            referencias.append(self.visitar(nodo))

        if len(referencias) > 0:
            return ",".join(referencias)
        else:
            return ""

    def __visitar_expresion_matematica(self, nodo_actual):
        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones.append(self.visitar(nodo))

        return " ".join(instrucciones)

    def __visitar_operacion(self, nodo_actual):
        """
        Operación ::= [Operando Operando Operador]
        """
        resultado = """{} {} {}"""
        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones.append(self.visitar(nodo))

        return resultado.format(instrucciones[0],
                                instrucciones[1],
                                instrucciones[2])

    def __visitar_operador(self, nodo_actual):
        """
        Operador ::= + | - | * | / | % | ^
        """
        return nodo_actual.contenido.string

    def __visitar_funcion(self, nodo_actual):
        """
        Función::= (Referencia?) Identificador pandemia delta Instrucción* Retorno omicron
        """
        resultado = """def {} ({}):\n{}"""
        instrucciones = [self.visitar(nodo_actual.nodos[0]), self.visitar(nodo_actual.nodos[1])]

        self.tabuladores += 1
        for nodo in nodo_actual.nodos[2:]:
            instrucciones += [("\t" * self.tabuladores) + self.visitar(nodo)]
        self.tabuladores -= 1

        return resultado.format(instrucciones[0], instrucciones[1], "\n".join(instrucciones[2:]))

    def __visitar_instruccion(self, nodo_actual):
        """
        Instrucción ::= (Comentario | Asignación | Lógica | Invocaciones)
        """
        return self.visitar(nodo_actual.nodos[0])

    def __visitar_condicion(self, nodo_actual):
        """
        Condición ::= Si Sino?
        """
        if (len(nodo_actual.nodos) == 1):
            return self.visitar(nodo_actual.nodos[0])
        else:
            return self.visitar(nodo_actual.nodos[0]) + "\n" + self.visitar(nodo_actual.nodos[1])


    def __visitar_si(self, nodo_actual):
        resultado = """if {}:\n{}"""
        instrucciones = []
        for nodo in nodo_actual.nodos:
            instrucciones.append(self.visitar(nodo))
        return resultado.format(instrucciones[0], instrucciones[1])

    def __visitar_sino(self, nodo_actual):
        resultado = """{}else:\n{} """

        return resultado.format(("\t" * self.tabuladores), self.visitar(nodo_actual.nodos[0]))

    def __visitar_comparacion(self, nodo_actual):
        resultado = """{} {} {}"""
        instrucciones = []

        for nodo in nodo_actual.nodos:
            instrucciones.append(self.visitar(nodo))

        return resultado.format(instrucciones[0], instrucciones[1], instrucciones[2])

    def __visitar_comparador(self, nodo_actual):
        comparador = nodo_actual.contenido.string

        if comparador == "menosCasos":
            return "<"
        elif comparador == "masCasos":
            return ">"
        elif comparador == "igualOMenosCasos":
            return "<="
        elif comparador == "igualOMasCasos":
            return ">="
        elif comparador == "mismosCasos":
            return "=="
        else:
            return "!="


    def __visitar_retorno(self, nodo_actual):
        resultado = """return {}"""

        return resultado.format(self.visitar(nodo_actual.nodos[0]))

    def __visitar__iteracion(self, nodo_actual):
        resultado = """while {}:\n{}"""

        return resultado.format(self.visitar(nodo_actual.nodos[0]), self.visitar(nodo_actual.nodos[1]))

    def __visitar_principal(self, nodo_actual):
        resultado = """if __name__ == '__main__':\n{}"""
        return resultado.format(self.visitar(nodo_actual.nodos[0]))

    def __visitar_ambiente_estandar(self, nodo_actual):
        return nodo_actual.contenido.string

    def __visitar_operando(self, nodo_actual):
        instruccion = self.visitar(nodo_actual.nodos[0])

        if instruccion != "":
            return instruccion
        else:
            return "\"\""

    def __visitar_variable(self, nodo_actual):
        return self.visitar(nodo_actual.nodos[0])

    def __visitar_contenido(self, nodo_actual):
        self.tabuladores += 1
        instrucciones = []
        for nodo in nodo_actual.nodos:
            instrucciones += [("\t" * self.tabuladores) + self.visitar(nodo)]
        self.tabuladores -= 1
        return "\n".join(instrucciones)

    def __visitar_identificador(self, nodo_actual):
        return nodo_actual.contenido.string









































