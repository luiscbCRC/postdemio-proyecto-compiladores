# En este archivo se maneja todo lo que tenga que ver con archivos externos al programa

class CargarArchivo:
  """
  Esta clase funciona para manejar todo el tema de archivos en el programa.

  ...

  Atributos
  ----------
  Ninguno

  Métodos
  -------
  archivo_a_lista(ruta_archivo:string):
    Busca un archivo y retorna una lista con su contenido
  """


  def archivo_a_lista(self, ruta_archivo):
    """
    A partir de la ruta del archivo, abre el archivo y va linea por linea intrododuciendo la linea en una lista

    Además se encarga de eliminar los espacios extra y tabulaciones que existan en la linea.

    Parámetros
    ----------
    ruta_archivo : string, requerido
      Ruta relativa de la ubicación del archivo que se desea leer de código Postdemio

    Retorno
    -------
    Una lista con el código del archivo
    """

    codigo_en_lista = []
    with open(ruta_archivo) as archivo:
      for linea in archivo:
        if not (linea == "\n" and len(linea) == 1):
          linea = ' '.join(linea.split())
        codigo_en_lista.append(linea)
      return codigo_en_lista
